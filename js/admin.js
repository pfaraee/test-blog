$(function () {
    // bekommt man von backendless.com project
    var APPLICATION_ID = "2067FBB3-2722-68FD-FFF8-9664390C0300",
        SECRET_KEY = "3397FF47-EDBF-DC11-FF0C-2B745F1CBE00",
        VERSION = "v1";

    // initializes our app
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);

    if (Backendless.UserService.isValidLogin()) {
        userLoggedIn(Backendless.LocalCache.get("current-user-id"));
    } else {
        var loginScript = $('#login-template').html();
        var loginTemplate = Handlebars.compile(loginScript);
        $('.main-container').html(loginTemplate);
    }

    console.log(Backendless.LocalCache.getAll());

    // Signin and Signout
    $(document).on('submit', '.form-signin', function (event) {
        event.preventDefault();

        var data = $(this).serializeArray(),
            email = data[0].value,
            password = data[1].value;

        Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
    });

    $(document).on('click', '.logout', function () {
        Backendless.UserService.logout(new Backendless.Async(userLoggedout, gotError));

        var loginScript = $('#login-template').html();
        var loginTemplate = Handlebars.compile(loginScript);
        $('.main-container').html(loginTemplate);
    });

    //Blog 
    $(document).on('click', '.add-blog', function () {
        var addBlogScript = $('#add-blog-template').html();
        var addBlogTemplate = Handlebars.compile(addBlogScript);
        $('.main-container').html(addBlogTemplate);
    });

    $(document).on('submit', '.form-add-blog', function (event) {
        event.preventDefault();

        var data = $(this).serializeArray(),
            title = data[0].value,
            content = data[1].value;

        var dataStore = Backendless.Persistence.of(Posts);

        var postObject = new Posts({
            title: title,
            content: content,
            authorEmail: Backendless.UserService.getCurrentUser().email
        });

        dataStore.save(postObject);

        this.title.value = "";
        this.content.value = "";
    });

});

// serves as a schema for our post data
// creates uniformity to make sure only the fields we want is inserted so there is no conflict when we work with our data
function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.authorEmail = args.authorEmail || "";
    this.content = args.content || "";
}

function userLoggedIn(user) {
    var userData;

    if (typeof user === "string") {
        userData = Backendless.Data.of(Backendless.User).findById(user);
    } else {
        userData = user;
    }

    var welcomeScript = $('#welcome-template').html();
    var welcomeTemplate = Handlebars.compile(welcomeScript);
    var welcomeHTML = welcomeTemplate(userData);
    $('.main-container').html(welcomeHTML);
}

function userLoggedout() {
    console.log("user has been logged out");
}

function gotError(error) {
    console.log("Error message - " + error.message);
    console.log("Error code - " + error.statusCode);
}