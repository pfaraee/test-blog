$(function () {
    // bekommt man von backendless.com project
    var APPLICATION_ID = "2067FBB3-2722-68FD-FFF8-9664390C0300",
        SECRET_KEY = "3397FF47-EDBF-DC11-FF0C-2B745F1CBE00",
        VERSION = "v1";

    // initializes our app
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);

    var postCollection = Backendless.Persistence.of(Posts).find();

    // check to see what is in our postCollection
    console.log(postCollection.data);

    var wrapper = {
        posts: postCollection.data
    };
    console.log(wrapper);

    var blogScript = $("#blogs-template").html();
    var blogsTemplate = Handlebars.compile(blogScript);
    var blogsHTML = blogsTemplate(wrapper);

    $('.main-container').html(blogsHTML);
});

// serves as a schema for our post data
// creates uniformity to make sure only the fields we want is inserted so there is no conflict when we work with our data
function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.authorEmail = args.authorEmail || "";
    this.content = args.content || "";
}